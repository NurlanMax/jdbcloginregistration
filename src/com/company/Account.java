package com.company;

import org.mindrot.jbcrypt.BCrypt;

public class Account {
    private Long id;
    private String login;
    private String password;
    private String mail;
    private String phoneNum;

    public Account(Long id,String login, String password, String mail, String phoneNum) {
        this.id=id;
        this.login = login;
        this.password = password;
        this.mail = mail;
        this.phoneNum = phoneNum;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        String hashed = BCrypt.hashpw(password, BCrypt.gensalt(12));
        return hashed;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", mail='" + mail + '\'' +
                ", phoneNum='" + phoneNum + '\'' +
                '}';
    }
}

