package com.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class DBManager {
    private Connection  connection;
    public void connect(){
        try {
            connection= DriverManager.getConnection
                    ("jdbc:postgresql://localhost:5432/socialnetaccount?useUnicode=true&serverTimezone=UTC", "postgres", "megatiger1998");
            System.out.println("Connected");
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    public void addAccount(Account account){
        if (getAccountByLogin(account.getLogin()) != null) {
            System.out.println("Account does not exist");
            return;
        }
        try {
            PreparedStatement st=connection.prepareStatement
                    ("INSERT INTO VK_ACCOUNT(AC_LOGIN,AC_PASSWORD,AC_MAIL,PHONE) VALUES(?,?,?,?)");
            st.setString(1,account.getLogin());
            st.setString(2,account.getPassword());
            st.setString(3,account.getMail());
            st.setString(4,account.getPhoneNum());
            st.executeUpdate();
            st.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


//    public ArrayList<Account> searchAccounts(String login,String password)
//    {
//        String query = "SELECT * FROM <tale> WHERE login= " + login + ", password " + password;
//
//        ArrayList<Account> accounts = new ArrayList<>();
//
//
//        PreparedStatement st=connection.prepareStatement(query);
//        while (st.next())
//
//        st.close();
//    }
    public ArrayList<Account> getAllAccount(){
        ArrayList<Account> accounts=new ArrayList<>();
        try {
            PreparedStatement st=connection.prepareStatement("SELECT * FROM VK_ACCOUNT");
            ResultSet rs=st.executeQuery();
            while (rs.next()){
                Long id=rs.getLong("ac_id");
                String login=rs.getString("ac_login");
                String password=rs.getString("ac_password");
                String mail=rs.getString("ac_mail");
                String phoneNum=rs.getString("phone");
                accounts.add(new Account(id,login,password,mail,phoneNum));
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return accounts;
    }
    public Account getAccountByLogin(String login) {
        Account account = null;
        try {
            PreparedStatement st = connection.prepareStatement("SELECT * FROM VK_ACCOUNT WHERE ac_login='" + login + "';");
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                account = new Account(rs.getLong("ac_id"), login, rs.getString("ac_password"), rs.getString("ac_mail"), rs.getString("phone"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return account;
    }
}
//"SELECT * FROM VK_ACCOUNT Where login='"\+ login +\"'"
//        ArrayList<Account> accounts=db.getAllAccount();
//        System.out.println("Insert login: ");
//        String existedLogin=sc.next();
//        System.out.println("Insert password" );
//        String existedPassword=sc.next();
//
//        Account currentAccount = null;
//
//        for (Account a: accounts) {
//        if(BCrypt.checkpw(existedPassword, a.getPassword())&& existedLogin.equals(a.getLogin())) {
//        currentAccount = a;
//        }
//        }