package com.company;

import org.mindrot.jbcrypt.BCrypt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        DBManager db=new DBManager();
        db.connect();

        while (true) {
            System.out.println("1-create an account");
            System.out.println("2-Sign in to account");
            int choice=sc.nextInt();
            if (choice==1){
                System.out.println("Create login: ");
                String login=sc.next();
                System.out.println("Create password: ");
                String password=sc.next();
//                String hashed = BCrypt.hashpw(password, BCrypt.gensalt(12));
                System.out.println("Insert your mail: ");
                String mail=sc.next();
                System.out.println("Insert your phone number: ");
                String phoneNum=sc.next();

                Account account = new Account(null,login,password,mail,phoneNum);
                db.addAccount(account);
            } else if(choice==2) {
//                ArrayList<Account> accounts=db.getAllAccount();
                System.out.println("Insert login: ");
                String existedLogin=sc.next();
                System.out.println("Insert password" );
                String existedPassword=sc.next();
                String rehashed = BCrypt.hashpw(existedPassword, BCrypt.gensalt(12));
                Account currentAccount = null;
                currentAccount = db.getAccountByLogin(existedLogin);

                if (currentAccount == null) {
                    System.out.println("Error, try again ");
                    continue;
                }
                if (BCrypt.checkpw(existedPassword, currentAccount.getPassword())){
                    System.out.println("Error");
                    continue;
                }
                System.out.println(currentAccount.toString());

//                System.out.println(currentAccount);



            }
        }
	// write your code here
    }
}
